package r8labs.button;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.*;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final TextView tv = new TextView(this);
        tv.setText("Press the button, please");

        Button b = new Button(this);
        b.setText("Press me");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText("Good!");
            }
        });

        LinearLayout l = new LinearLayout(this);
        l.addView(b);
        l.addView(tv);
        setContentView(l);
    }
}